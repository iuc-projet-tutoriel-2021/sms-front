export const state = () => ({
  user: null,
  authenticated: false,
})

export const getters = {
  user: state => state.user,
}

export const mutations = {
  set_user: (state, payload) => state.user = payload
}

export const actions = {
  async get_user({commit}) {
    const token = this.$cookies.get('token')
    if (token) {
      this.$axios.defaults.headers = {
        Authorization: "Bearer " + token,
        ...this.$axios.defaults.headers
      }
      const {data} = await this.$axios.get('/user')
      commit('set_user', data)
      return Promise.resolve(data)
    }
  }
}
