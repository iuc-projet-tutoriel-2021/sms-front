export default function ({store, redirect}) {
  return new Promise(resolve => store.dispatch('get_user').then((user) => {
    if (!user) {
      redirect('/auth/login')
    } else {
      return resolve()
    }
  }))
}
