import Vue from 'vue'

Vue.mixin({
  data() {
    return {
      global_rules: {
        required: value => !!value && value.length > 0 || 'Ce champs est réquis.',
        counter: value => value.length <= 20 || 'Max 20 characters',
        email: value => {
          const pattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
          return pattern.test(value) || 'Le format email doit être valide.'
        },
        tel: value => /^\+([\d]{3})\d+/.test(value) || "Votre numéro doit porter l'indicatif de votre pays. Ex: +237 6********"
      },
    }
  },
  methods: {
    is_admin: (user) => user && user.role === 'admin'
  }
})
